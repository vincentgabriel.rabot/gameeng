﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = System.Random;
//using UnityEngine.Windows.Speech;

public class Block : MonoBehaviour
{
    public static int height = 20;
    public static int width = 10;
    private static Transform[,] grid = new Transform[width,height];

    public Vector3 rotationPoint;
    private float previousTime;
    public float fallTime;
    [SerializeField] List<GameObject> gameObjects = new List<GameObject>();
   //private KeywordRecognizer keywordRecognizer;
    private Dictionary<string, Action> actions = new Dictionary<string, Action>();

    // Start is called before the first frame update
    void Start()
    {
        //actions.Add("clear", ClearArray);

       //keywordRecognizer = new KeywordRecognizer(actions.Keys.ToArray());
        //keywordRecognizer.OnPhraseRecognized += RecognizedSpeech;
        //keywordRecognizer.Start();
    }

    void AddToGrid()
    {
        foreach (Transform children in transform)
        {
            int roundedX = Mathf.RoundToInt(children.transform.position.x);
            int roundedY = Mathf.RoundToInt(children.transform.position.y);

            grid[roundedX, roundedY] = children;
        }
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
        Rotate();
        //transform.Translate(new Vector3(0, -5 * Time.deltaTime, 0));

        if (Time.time - previousTime > (Input.GetKey(KeyCode.DownArrow) ? fallTime / 10 : fallTime))
        {
            transform.position += new Vector3(0, -1, 0);
            if (!GameManager.instance.ValidMove(this.gameObject))
            {
                transform.position -= new Vector3(0, -1, 0);
                GameManager.instance.AddToGrid(this.gameObject);
                GameManager.instance.CheckForLines();
                this.enabled = false;
                TimedSpawn.instance.NewBlock();
            }

            previousTime = Time.time;
        }
    }

    void Movement()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.acceleration.x < -0.2f)
        {
            transform.position += new Vector3(-1, 0, 0);
            if(!GameManager.instance.ValidMove(this.gameObject))
                transform.position -= new Vector3(-1, 0, 0);
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow) || Input.acceleration.x > 0.2f)
        {
            transform.position += new Vector3(1, 0, 0);
            if(!GameManager.instance.ValidMove(this.gameObject))
                transform.position -= new Vector3(1, 0, 0);
        }

    }

    public void Rotate()
    {
        if (MicInput.MicLoudnessinDecibels >= -5 && MicInput.Instance._canShout || Input.GetKeyDown(KeyCode.UpArrow))
        {
            transform.RotateAround(transform.TransformPoint(rotationPoint), new Vector3(0, 0, 1), 90);
            if (!GameManager.instance.ValidMove(this.gameObject))
                transform.RotateAround(transform.TransformPoint(rotationPoint), new Vector3(0, 0, 1), -90);
            
            StartCoroutine(Delay());
        }
    }

    IEnumerator Delay()
    {
        MicInput.Instance._canShout = false;
        yield return new WaitForSeconds(1);
        MicInput.Instance._canShout = true;
    }

   
    //private void RecognizedSpeech(PhraseRecognizedEventArgs speech)
    //{
        //Debug.Log(speech.text);
        //actions[speech.text].Invoke();
    //}

    private void ClearArray()
    {
        transform.Translate(1, 0 , 0);
    }
    

}
