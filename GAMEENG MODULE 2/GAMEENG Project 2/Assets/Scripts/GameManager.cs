﻿using System.Collections;
using System.Collections.Generic;
using System.Resources;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public int score;
    public int combo;
    public static int height = 21;
    public static int width = 11;

    private int clearValue;
    private int total;

    private static Transform[,] grid = new Transform[width,height];
    public static GameManager instance = null;


    [Header("Game Related")]
    public GameObject GamePanel;
    public GameObject GameOver;
    public Text scoreText;
    public Text comboText;
    public GameObject MainMenuPanel;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            Destroy(gameObject);
        }
    }

    public bool ValidMove(GameObject block)
    {
        foreach (Transform children in block.transform)
        { 
            int roundedX = Mathf.RoundToInt(children.transform.position.x);
            int roundedY = Mathf.RoundToInt(children.transform.position.y);
            
            if (roundedX < 0 || roundedX >= width || roundedY < 0 || roundedY >= height)
            {
                return false;
            }

            if (grid[roundedX, roundedY] != null)
                return false;


        }    
        return true;
    }

    public void AddToGrid(GameObject block)
    {
        foreach (Transform children in block.transform)
        {
            int roundedX = Mathf.RoundToInt(children.transform.position.x);
            int roundedY = Mathf.RoundToInt(children.transform.position.y);
            
            if (roundedY >= 19f)
            {
                TimedSpawn.instance.canSpawn = false;
                //trigger game over
                OnPlayerDeath();
            }
            
            grid[roundedX, roundedY] = children;
        }
    }

    public void CheckForLines()
    {
        for (int i = height - 1; i >= 0; i--)
        {
            if (HasLine(i))
            {
                DeleteLine(i);
                RowDown(i);
            }
        }
    }

    public bool HasLine(int i)
    {
        for (int j = 0; j < width; j++)
        {
            if (grid[j, i] == null)
                return false;
        }

        return true;
    }

   public void DeleteLine(int i)
    {
        for (int j = 0; j < width; j++)
        {
            Destroy(grid[j,i].gameObject);
            grid[j, i] = null;
        }

        combo += 1;
        clearValue = 1000;
        total = combo * clearValue;
        UpdateScore(total);    
    }

    public void RowDown(int i)
    {
        for (int y = i; y < height; y++)
        {
            for (int j = 0; j < width; j++)
            {
                if (grid[j, y] != null)
                {
                    grid[j, y - 1] = grid[j, y];
                    grid[j, y] = null;
                    grid[j,y - 1].transform.position -= new Vector3(0,1,0);
                }
            }
        }
    }

    public void OnStartClicked()
    {
        GameManager.instance.GamePanel.SetActive(true);
        GameManager.instance.MainMenuPanel.SetActive(false);
    }

    public void OnPlayerDeath()
    {
        GamePanel.SetActive(false);
        GameOver.SetActive(true);
        foreach (GameObject block in TimedSpawn.instance.spawnedBlocks)
        {
            Destroy(block);
        }
        TimedSpawn.instance.spawnedBlocks.Clear();
        Destroy(TimedSpawn.instance.gameObject);
        scoreText.text = "Score: " + score;
    }

    public void UpdateScore(int value)
    {
        score += value;
        scoreText.text = "Score: " + score;
        comboText.text = "Combo: " + combo;
    }
}
