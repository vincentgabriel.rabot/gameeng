using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
 
public class MicInput : MonoBehaviour
{
    #region SingleTon
 
    public static MicInput Instance { set; get; }
 
    #endregion
 
    public static float MicLoudness;
    public static float MicLoudnessinDecibels;
    public bool _canShout = true;
    private string _device;
    public Text dbText;
    bool _isInitialized;    
    AudioClip _clipRecord;
    AudioClip _recordedClip;
    int _sampleWindow = 128;

    //mic initialization
    public void InitMic()
    {
        if (_device == null)
        {
            _device = Microphone.devices[0];
        }
        _clipRecord = Microphone.Start(_device, true, 999, 44100);
        _isInitialized = true;
    }
 
    public void StopMicrophone()
    {
        Microphone.End(_device);
        _isInitialized = false;
    }

    //get data from microphone into audioclip
    float MicrophoneLevelMax()
    {
        float levelMax = 0;
        float[] waveData = new float[_sampleWindow];
        int micPosition = Microphone.GetPosition(null) - (_sampleWindow + 1); // null means the first microphone
        if (micPosition < 0) return 0;
        _clipRecord.GetData(waveData, micPosition);
        // Getting a peak on the last 128 samples
        for (int i = 0; i < _sampleWindow; i++)
        {
            float wavePeak = waveData[i] * waveData[i];
            if (levelMax < wavePeak)
            {
                levelMax = wavePeak;
            }
        }
        return levelMax;
    }
 
    //get data from microphone into audioclip
    float MicrophoneLevelMaxDecibels()
    {
 
        float db = 20 * Mathf.Log10(Mathf.Abs(MicLoudness));
 
        return db;
    }
 
    public float FloatLinearOfClip(AudioClip clip)
    {
        StopMicrophone();
 
        _recordedClip = clip;
 
        float levelMax = 0;
        float[] waveData = new float[_recordedClip.samples];
 
        _recordedClip.GetData(waveData, 0);
        // Getting a peak on the last 128 samples
        for (int i = 0; i < _recordedClip.samples; i++)
        {
            float wavePeak = waveData[i] * waveData[i];
            if (levelMax < wavePeak)
            {
                levelMax = wavePeak;
            }
        }
        return levelMax;
    }
 
    public float DecibelsOfClip(AudioClip clip)
    {
        StopMicrophone();
 
        _recordedClip = clip;

 
        float levelMax = 0;
        float[] waveData = new float[_recordedClip.samples];
 
        _recordedClip.GetData(waveData, 0);
        // Getting a peak on the last 128 samples
        for (int i = 0; i < _recordedClip.samples; i++)
        {
            float wavePeak = waveData[i] * waveData[i];
            if (levelMax < wavePeak)
            {
                levelMax = wavePeak;
            }
        }
 
        float db = 20 * Mathf.Log10(Mathf.Abs(levelMax));
 
        return db;
    }
 
    void Update()
    {
        // levelMax equals to the highest normalized value power 2, a small number because < 1
        // pass the value to a static var so we can access it from anywhere
        MicLoudness = MicrophoneLevelMax();
        MicLoudnessinDecibels = MicrophoneLevelMaxDecibels();
    }
    
    // start mic when scene starts
    void OnEnable()
    {
        InitMic();
        _isInitialized = true;
        Instance = this;
    }
 
    //stop mic when loading a new level or quit application
    void OnDisable()
    {
        StopMicrophone();
    }
 
    void OnDestroy()
    {
        StopMicrophone();
    }
 
    // make sure the mic gets started & stopped when application gets focused
    void OnApplicationFocus(bool focus)
    {
        if (focus)
        {
            if (!_isInitialized)
            {
                InitMic();
            }
        }
        if (!focus)
        {
            StopMicrophone();
 
        }
    }

    private float DecibelToLinear(float dB)
    {
        float linear = Mathf.Pow(10.0f, dB/20.0f);
        return linear;
    }

    private float LinearToDecibel(float linear)
    {
        float dB;
        if (linear != 0)
        dB = 20.0f * Mathf.Log10(linear);
        else
        dB = -144.0f;
        return dB;
    }
 
}
 
 