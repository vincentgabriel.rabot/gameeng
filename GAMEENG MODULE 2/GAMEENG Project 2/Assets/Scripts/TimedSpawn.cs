﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedSpawn : MonoBehaviour
{
    public GameObject[] Blocks;
    public static TimedSpawn instance;
    public List<GameObject> spawnedBlocks;
    public bool canSpawn = true;


    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    void Start()
    {
        NewBlock();
    }

    public void NewBlock()
    {
        if (canSpawn)
        {
            GameObject spawnedBlock = Instantiate(Blocks[Random.Range(0, Blocks.Length)], transform.position,
                Quaternion.identity);
            spawnedBlocks.Add(spawnedBlock);
        }
        else if (!canSpawn)
        {
            foreach (GameObject blocks in spawnedBlocks)
            {
                Destroy(blocks);
            }
        }
        
    }
    
}
