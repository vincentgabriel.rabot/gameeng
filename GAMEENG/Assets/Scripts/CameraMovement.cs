using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public GameObject originalPosition;
    private Vector3 tempPosition;
    private bool hasResetted = false;
    int speed = -5;

    
    void Start()
    {
        tempPosition = originalPosition.transform.position;
    }
    void LateUpdate()
    {
        if(hasResetted == false) {
            StartCoroutine(ResetBackground());
        }
        transform.Translate(speed * Time.deltaTime, 0, 0);
    }

    IEnumerator ResetBackground() 
    {
        hasResetted = true;
        yield return new WaitForSeconds(8f);
        transform.position = tempPosition;
        hasResetted = false;
    }
}
