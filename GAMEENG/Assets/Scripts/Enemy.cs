using System.Collections;
using System.Collections.Generic;
using UnityEditor.Networking.PlayerConnection;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float moveSpeed;

    void Update()
    {
        transform.Translate(-moveSpeed * Time.deltaTime, 0, 0);
        if (this.transform.position.x <= -13)
        {
            EnemySpawner.instance.spawnedEnemies.Remove(this.gameObject);
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D (Collider2D col)
    {
        if(col.CompareTag("Player")) {
            col.GetComponent<PlayerHealth>().TakeDamage(1);
            Destroy(gameObject);
        }
    }
}
