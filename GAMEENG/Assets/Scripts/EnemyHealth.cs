using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : Health
{
    public GameObject healthBarUI;
    public Slider slider;
    public GameObject soundManager;
    public int score;

    void Start()
    {
        maxHealth = Random.Range(50, 200);
        currentHealth = 0;
        slider.value = CalculateHealth();
        soundManager = GameObject.Find("SoundManager");
    }
    
    void Update()
    {
        
    }

    public override void TakeDamage(int damage)
    {
        currentHealth += damage;
        //healthBarUI.SetActive(true);
        if (currentHealth >= maxHealth)
        {
            Die();
        }

        slider.value = CalculateHealth();
    }

    public void Die()
    {
        EnemySpawner.instance.spawnedEnemies.Remove(this.gameObject);
        score = Random.Range(10, 100);
        GameManager.instance.UpdateScore(score);
        healthBarUI.SetActive(false);
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<BoxCollider2D>().enabled = false;
        soundManager.GetComponent<SoundManager>().EnemyDeathSfx();
        Destroy(gameObject, 1f);
    }

    float CalculateHealth()
    {
        return currentHealth / maxHealth;
    }
}
