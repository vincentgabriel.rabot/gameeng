using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemySpawner : MonoBehaviour
{
    public static EnemySpawner instance;
    public GameObject[] enemyTypes;
    public GameObject obstaclePrefab;
    public List<GameObject> spawnedEnemies = new List<GameObject>();
    private Vector2 spawnLocation;
    private float nextSpawn = 0;
    private float minSpawnRate = 0.5f;
    private int spawnChance;
    private int randomSpawnNumber;
    private bool canSpawn = true;
    float maxSpawnRate = 2f;


    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }
    
    void Update()
    {
        if (Time.time > nextSpawn)
        {
            canSpawn = true;
        }

        if (canSpawn)
        {
            randomSpawnNumber = Random.Range(0, 101);
            if (randomSpawnNumber >= 90)
            {
                SpawnObstacle();
            }
            else
            {
                SpawnEnemies();
            }
            canSpawn = false;
        }
    }

    void SpawnObstacle()
    {
        nextSpawn = Time.time + Random.Range(minSpawnRate, maxSpawnRate);
        GameObject spawnedObstacle = Instantiate(obstaclePrefab, this.transform.position, Quaternion.identity);
        spawnedObstacle.transform.position = new Vector3(spawnedObstacle.transform.position.x, (float)-2.334, 0);
    }

    void SpawnEnemies()
    {
        if (spawnedEnemies.Count < 5)
        {
            nextSpawn = Time.time + Random.Range(minSpawnRate, maxSpawnRate);
            GameObject spawnedEnemy = Instantiate(enemyTypes[Random.Range(0, 3)], this.transform.position,
                    Quaternion.identity);
            spawnedEnemies.Add(spawnedEnemy);
        }
        else
        {
            return;
        }
    }
}
