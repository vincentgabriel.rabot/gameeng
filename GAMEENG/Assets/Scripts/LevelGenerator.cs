using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{

    const float playerDistance = 20f;
    public GameObject wallPrefabs;
    public float xSpawn = 0;
    public float tileLength;
    public int numberOfTiles = 5;
    private List<GameObject> activeTiles = new List<GameObject>();

    
    public Transform playerTransform;

    Vector3 lastPosition = new Vector2 (-10 , -3);

    void Start()
    {
        for (int i = 0; i < numberOfTiles; i++)
        {
            spawnTile();
        }
    }


    public void spawnTile()
    {
        GameObject active = Instantiate(wallPrefabs, lastPosition + (transform.right * xSpawn), Quaternion.identity);
        activeTiles.Add(active);
        xSpawn += tileLength;
    }

    void Update()
    {
        if (playerTransform.position.x > xSpawn - (numberOfTiles * tileLength))
        {
            spawnTile();
            DeleteTile();
        }
    }

    private void DeleteTile()
    {
        Destroy(activeTiles[0]);
        activeTiles.RemoveAt(0);
    }

}
