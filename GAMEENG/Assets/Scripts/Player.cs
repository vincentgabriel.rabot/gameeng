using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.Windows.WebCam;

public class Player : MonoBehaviour
{
    public float speed;
    public bool swiped;
    public GameObject projectilePrefab;
    Vector2 firstPos, lastPos;
    float jumpForce = 300f;
    private Rigidbody2D rb;
    public Swipe.Direction direction;

    bool isGrounded = true;

    public static Player instance;
    private void Awake()
    {
        if(instance != null)
        {
            Destroy(this.gameObject);
        }
        else 
        {
            instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.position.x <= -13)
        {
            Destroy(this.gameObject);
            GameManager.instance.OnPlayerDeath();
        }
        #if UNITY_EDITOR || UNITY_STANDALONE
        if (Input.GetMouseButtonDown(0))
        {
            firstPos = Input.mousePosition;
            
        }

        if (Input.GetMouseButtonUp(0))
        {
            lastPos = Input.mousePosition;


            if (firstPos != lastPos) {
                direction = Swipe.direction(firstPos, lastPos);
                //Debug.Log("Swiped = true");
                swiped = true;
                
                // swipe move direction up and down
                /*if (direction == Swipe.Direction.down)
                {
                    transform.position = new Vector2(this.transform.position.x, this.transform.position.y - 1.75f);
                }*/

                if (direction == Swipe.Direction.up && isGrounded == true)
                {
                    //transform.position = new Vector2(this.transform.position.x, this.transform.position.y + 1.75f);
                    rb.AddForce (Vector2.up * jumpForce);
                    isGrounded = false;
                }
            }
            else
            {
                Shoot();
            }
        }

        else
        {
            swiped = false;
        }
           
#elif UNITY_ANDROID || UNITY_IOS
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            switch (touch.phase)
            {
                case TouchPhase.Began
                    startPos = touch.position;
                    endPos = touch.position;
                    break;
                
                case TouchPhase.Moved
                    direction = touch.position - startPos;
                    break;

                case TouchPhase.ended
                    endPos = touch.position;
                    swiped = true;

                    if(startPos != endPos) {
                        direction = Swipe.direction(firstPos, lastPos);
                        if (direction == Swipe.Direction.up) {
                            //transform.position = new Vector2(this.transform.position.x, this.transform.position.y + 1.75f);
                            rb.AddForce (Vector2.up * jumpForce);
                        }
                    }
                    break;
            }
        }
        else {
            Shoot();
        }
#endif
     

        //transform.Translate(speed * Time.deltaTime, 0, 0);
        //transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0));
    }

    void Shoot()
    {
        GameObject projectile = ObjectPool.SharedInstance.GetPooledObject();
        if (projectile != null)
        {
            projectile.transform.position = this.transform.position;
            projectile.transform.rotation = this.transform.rotation;
            projectile.SetActive(true);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Floor"))
        {
            isGrounded = true;
        }
    }
}
