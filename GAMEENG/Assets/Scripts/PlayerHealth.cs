using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : Health
{
    //public int health;
    public int numOfHearts;
    public Image[] hearts;
    public Sprite fullHeart;
    // Start is called before the first frame update
    void Start()
    {
        HealthInit();
    }

    public void HealthInit()
    {
        for (int i = 0; i < hearts.Length; i++)
        {
            if (i < currentHealth)
            {
                hearts[i].enabled = true;
            }
            else
            {
                hearts[i].enabled = false;
            }
        }
    }
    public override void TakeDamage(int damage)
    {
        currentHealth -= damage;
        HealthInit();
        if (currentHealth <= 0) 
        {
            GameManager.instance.OnPlayerDeath();
        }
    }
}
