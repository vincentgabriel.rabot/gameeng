using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneControl : MonoBehaviour
{
    public void OnStartClicked()
    {
        GameManager.instance.GamePanel.SetActive(true);
        GameManager.instance.MainMenuPanel.SetActive(false);
        GameManager.instance.GameOver.SetActive(false);
    }

    public void OnRetry()
    {
        SceneManager.LoadScene("Game");
    }

    public void OnReturnClicked()
    {
        GameManager.instance.MainMenuPanel.SetActive(true);
        GameManager.instance.GameOver.SetActive(false);
    }
    
    public void OnQuitClicked()
    {
        Application.Quit();
    }
}
