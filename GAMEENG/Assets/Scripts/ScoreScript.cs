using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour
{
    //public static ScoreScript instance;
    private int ScoreValue;
    private Text scoreText;
    
    // Start is called before the first frame update
    void Start()
    {
        
        scoreText = GetComponent<Text>();
    }

    public void UpdateScore(int value)
    {
        GameManager.instance.score += value;
        scoreText.text = "Score: " + GameManager.instance.score;
    }

    public void FinalScore(Text finalScoreText)
    {
        finalScoreText.text = "Congratulations! your final score is: " + GameManager.instance.score;
    }
    
    // Update is called once per frame
    void Update()
    {
        
    }
}
