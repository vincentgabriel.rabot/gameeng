using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Swipe : MonoBehaviour
{
    public enum Direction
    {
        up = 0,
        down = 1,
        right = 2,
        left = 3
    };

    public static Direction direction(Vector2 firstPos, Vector2 lastPos)
    {
        if (Mathf.Abs(lastPos.x - firstPos.x) > Mathf.Abs(lastPos.y - firstPos.y))
        {
            if (lastPos.x > firstPos.x)
            {
                Debug.Log("Right");
                return Direction.right;
            }

            else
            {
                Debug.Log("Left");
                return Direction.left;
            }
        }

        else
        {
            if (lastPos.y > firstPos.y)
            {
                Debug.Log("Up");
                return Direction.up;
            }

            else
            {
                Debug.Log("Down");
                return Direction.down;
            }
        }
    }
}

