using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    [Header("Game Related")]
    public GameObject GamePanel;
    public GameObject GameOver;
    public GameObject ScoreText;
    
    private void Awake()
    {
        if(instance != null)
        {
            Destroy(this.gameObject);
        }
        else 
        {
            instance = this;
        }
    }
    public void OnPlayerDeath()
    {
        GamePanel.SetActive(false);
        GameOver.SetActive(true);
    }

    public void UpdateScore(int value)
    {
        ScoreText.GetComponent<ScoreScript>().UpdateScore(value);
    }
}