using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float speed;
    public bool swiped;
    Vector2 firstPos, lastPos;
    public Swipe.Direction direction;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        #if UNITY_EDITOR || UNITY_STANDALONE
        if (Input.GetMouseButtonDown(0))
        {
            firstPos = Input.mousePosition;
            
        }

        if (Input.GetMouseButtonUp(0))
        {
            lastPos = Input.mousePosition;
         

            if (firstPos != lastPos) {
                direction = Swipe.direction(firstPos, lastPos);
                Debug.Log("Swiped = true");
                swiped = true;
            }
               
        }

        else {
            swiped = false;
        }
           
#elif UNITY_ANDROID || UNITY_IOS
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            switch (touch.phase)
            {
                case TouchPhase.Began
                    startPos = touch.position;
                    endPos = touch.position;
                    break;
                
                case TouchPhase.Moved
                    direction = touch.position - startPos;
                    break;

                case TouchPhase.ended
                    endPos = touch.position;
                    swiped = true;

                    if(startPos != endPos) {
                        direction = Swipe.direction(firstPos, lastPos);
                    }
                    break;
            }
        }
        else {
            swiped = false;
        }
#endif
     

        transform.Translate(0, speed * Time.deltaTime, 0);
        transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0));
    }
}
