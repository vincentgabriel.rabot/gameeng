using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public int score;

    [Header("Game Related")]
    public GameObject GamePanel;
    public GameObject GameOver;
    public GameObject scoreText;
    public GameObject finalScoreText;
    
    private void Awake()
    {
        if(instance != null)
        {
            Destroy(this.gameObject);
        }
        else 
        {
            instance = this;
        }
    }
    public void OnPlayerDeath()
    {
        GamePanel.SetActive(false);
        GameOver.SetActive(true);
        finalScoreText.GetComponent<ScoreScript>().FinalScore(finalScoreText.GetComponent<Text>());
    }

    public void UpdateScore(int value)
    {
        scoreText.GetComponent<ScoreScript>().UpdateScore(value);
    }
}